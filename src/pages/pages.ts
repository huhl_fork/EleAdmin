import { defineAsyncComponent } from 'vue';
import { ElEmpty } from 'element-plus';

const Main = defineAsyncComponent(() => import('./MainPage.vue'));
const Menu = defineAsyncComponent(() => import('../components/menu/EpxMenu.vue'));
const Form = defineAsyncComponent(() => import('./FormPage.vue'));
const Curd = defineAsyncComponent(() => import('./CurdPage.vue'));
const Maker = defineAsyncComponent(() => import('../components/maker/MakerMain.vue'));
const M2 = defineAsyncComponent(() => import('./BenchPage.vue'));

const Pages = { Main, Menu, Form, Curd, Maker, M2 };

export type PageName = keyof typeof Pages;

export const getPage = (name: PageName) => Pages[name] || ElEmpty;
