/**
 * # 表格文件
 */
import { Align, Er, Erx, Face, Row } from '../elementui';
import { Toolbar } from '../button/button';
import { Form } from '../form/form';
import { CellName } from './cells';

/**
 * ## 表格
 */
export interface Table extends Er {
  columns: Column[];
  selector?: Selectable;
  rows?: Row[];
}

export interface Curd extends Table {
  search?: Form;
  pagination?: Pagination;
  toolbar?: Toolbar['items'];
  embedment?: Toolbar['items'];
}

/**
 * ### 表格列
 */
export interface Column extends Er {
  /**
   * 列组件名称
   */
  cell?: CellName;
  /**
   * 固定位置
   */
  fixed?: true | 'left' | 'right';
  /**
   * 可排序
   */
  sortable?: boolean;
  /**
   * 表格内容对齐
   */
  align?: Align;
  /**
   * 表头对齐
   */
  headerAlign?: Align;

  // for url
  /**
   * 关联的URL列
   */
  relation?: string;

  // for tag
  sensor?: string;
  face?: Face;
  lookup?: Array<number>;

  // for pic
  width?: number | string;
  height?: string;
}

/**
 * 选择列
 */
export type Selectable = boolean;

/**
 * ## 分页
 */
export interface Pagination {
  size?: number;
  total?: number;
  current?: number;

  /**
   * 组件布局，子组件名用逗号分隔
   *     sizes / prev / pager / next / jumper / -> / total / slot
   * 'prev, pager, next, jumper, ->, total'
   */
  layout?: string;
  /**
   * 每页显示个数选择器的选项设置	number[]	—	[10, 20, 30, 40, 50, 100]
   */
  pageSizes?: number[];
  /**
   * 替代图标显示的上一页文字
   */
  prevText?: string;
  /**
   * 替代图标显示的下一页文字
   */
  nextText?: string;
  /**
   * 只有一页时是否隐藏
   */
  hideOnSinglePage?: boolean;
}

export interface TableResult {
  pagination?: Pagination;
  rows: Row[];
}

/**
 * ### 单元格
 */
export type Cell = Erx;

/**
 * 单元格定义
 */
export const cells: Record<CellName, Cell> = {
  icon: {
    icon: 'icon-app',
    label: '图标',
    name: 'icon'
  },
  pic: {
    icon: 'icon-pic',
    label: '图片',
    name: 'pic'
  },
  url: {
    icon: 'icon-link',
    label: '地址',
    name: 'url'
  },
  tag: {
    icon: 'icon-span',
    label: '标签',
    name: 'tag'
  },
  span: {
    icon: 'icon-char',
    label: '文字',
    name: 'span'
  }
};
