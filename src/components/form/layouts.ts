import { defineAsyncComponent } from 'vue';

const Rowed = defineAsyncComponent(() => import('./layout/RowLayout'));
const Simple = defineAsyncComponent(() => import('./layout/SimpleLayout'));

const Draggable = defineAsyncComponent(() => import('./layout/Draggable'));

const components = {
  Draggable,
  Simple,
  Rowed
};

export type FormLayoutType = keyof typeof components;

export const getLayout = (name: FormLayoutType) => components[name] || Rowed;
