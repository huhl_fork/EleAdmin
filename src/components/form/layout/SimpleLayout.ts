//  ====================== ElRow-ElCol ========================

import {
  arrayCreator,
  coreCreator,
  FieldEvents,
  FormEvents,
  getLayoutComponent,
  groupCreator,
  labelCreator,
  mainCreator,
  Wrapper,
  WrapperParam,
  YNode
} from './wrapper';
import { addNode, deleteNode } from './RowLayout';
import { Field } from '../form';
import { computed } from 'vue';
import { Row } from '../../elementUI';

export const coreWrapper: Wrapper<FieldEvents, Field, YNode[]> = (param) => coreCreator(param);

export const groupWrapper: Wrapper<FieldEvents> = (param) => {
  const { field } = param;

  if (!field.fields) return coreWrapper(param);

  const labelNode = labelCreator(param);

  const children = groupCreator(param);

  return labelNode ? labelNode.concat(children) : children;
};

export const arrayWrapper: Wrapper<FieldEvents> = (param) => {
  const { handler, children } = arrayCreator(param);

  let nodes: YNode[] = [];

  children.forEach((node: YNode[] | undefined, index: number) => {
    if (!node) return;
    nodes.concat(node);
    nodes.push(deleteNode(() => handler?._array_remove(index)));
  });

  nodes = nodes.concat([addNode(handler?._array_add)] || []);

  const node = labelCreator(param);
  if (node) nodes = node.concat(nodes);

  return nodes;
};

// noinspection JSUnusedGlobalSymbols
export default getLayoutComponent((props, { emit, slots, attrs }) => {
  const param = computed<WrapperParam<FormEvents, Field[]>>(() => {
    return {
      field: props.fields || [],
      value: props.modelValue as Row,
      events: {
        modelValue: (v) => emit('update:modelValue', v),
        elements: (v) => emit('update:elements', v)
      },
      attrs,
      context: {
        slots,
        wrappers: {
          groupWrapper: groupWrapper,
          coreWrapper: coreWrapper,
          arrayWrapper: arrayWrapper,
          mainWrapper: mainCreator
        }
      }
    } as WrapperParam<FormEvents, Field[]>;
  });

  return mainCreator(param.value);
});
