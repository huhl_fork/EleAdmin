import { MockMethod } from 'vite-plugin-mock';
import { Result, View } from '../src/components/core';
import { Menu } from '../src/components/menu/menu';

// noinspection JSUnusedGlobalSymbols
export default [
  {
    url: '/api/index/index',
    method: 'get',
    response: () => {
      return {
        value: {
          view: 'Main',
          menu: '/api/index/menu'
        } as View
      } as Result;
    }
  },
  {
    url: '/api/index/menu',
    method: 'get',
    response: () => {
      return {
        value: {
          view: 'Menu',
          items: [
            {
              label: '构建工具',
              name: 'creator',
              id: 1,
              icon: 'IEpOfficeBuilding',
              items: [
                {
                  label: '表单构建',
                  name: 'form-creator',
                  icon: 'IEpTickets',
                  id: 11,
                  command: {
                    url: '/api/maker/form'
                  }
                },
                {
                  label: '表格构建',
                  name: 'table-creator',
                  id: 12,
                  command: {
                    url: '/api/maker/table'
                  },
                  icon: 'IEpGrid'
                },
                {
                  label: '图表构建',
                  name: 'chart-creator',
                  id: 13,
                  command: {
                    url: '/api/maker/chart'
                  },
                  icon: 'IEpHistogram'
                },
                {
                  label: '模型构建',
                  name: 'model-creator',
                  id: 14,
                  command: {
                    url: '/api/maker/model'
                  },
                  icon: 'IEpCpu'
                }
              ]
            },
            {
              label: '高级组件',
              name: 'components',
              icon: 'IEpCoin',
              id: 2,
              items: [
                {
                  label: '高级表单',
                  name: 'form',
                  icon: 'IEpCellphone',
                  id: 21,
                  command: {
                    url: '/api/form/index'
                  }
                },
                {
                  label: '高级表格',
                  name: 'table',
                  icon: 'IEpCreditCard',
                  id: 22,
                  command: {
                    url: '/api/table/index'
                  }
                }
              ]
            },
            {
              label: '模块管理',
              icon: 'IEpGuide',
              name: 'guide',
              id: 3,
              command: {
                url: '/api/maker/module'
              }
            },
            {
              label: '数独计算',
              name: 'some',
              icon: 'IEpBox',
              id: 5,
              command: {
                url: '/api/suduku/index'
              }
            }
          ] as Array<Menu>
        } as View
      } as Result;
    }
  },
  {
    url: '/api/slot/index',
    method: 'get',
    response: () => {
      return {
        value: {
          view: 'Text'
        } as View
      } as Result;
    }
  },
  {
    url: '/api/suduku/index',
    method: 'get',
    response: () => {
      return {
        value: {
          view: 'Suduku'
        } as View
      } as Result;
    }
  }
] as MockMethod[];
