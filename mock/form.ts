import { MockMethod } from 'vite-plugin-mock';
import { Form } from '../src/components/form/form';
import { View } from '../src/components/core';

const form: Form = {
  label: '新增用户',
  description: '这里是一段填表说明',
  style: {
    width: '1180px'
  },
  size: 'default',
  labelWidth: '100px',
  labelPosition: 'left',
  fields: [
    {
      name: 'name',
      label: '姓名',
      widget: 'input'
      // col: 12
    },
    {
      name: 'address',
      label: '地址',
      widget: 'input',
      arrayed: true
    },
    {
      name: 'sex',
      label: '性别',
      widget: 'radio',
      // col: 12,
      options: [
        {
          label: '男',
          value: 1
        },
        {
          label: '女',
          value: 0
        }
      ]
    },
    {
      name: 'hobby',
      label: '爱好',
      widget: 'checkbox',
      // col: 12,
      options: [
        {
          label: '游泳',
          value: 1
        },
        {
          label: '羽毛球',
          value: 2
        },
        {
          label: '乒乓球',
          value: 4
        }
      ]
    },
    {
      label: '甜点',
      name: 'dessert',
      widget: 'select',
      col: 10,
      options: [
        {
          value: 1,
          label: '黄金糕'
        },
        {
          value: 2,
          label: '双皮奶'
        },
        {
          value: 3,
          label: '蚵仔煎'
        },
        {
          value: 4,
          label: '龙须面'
        },
        {
          value: 5,
          label: '北京烤鸭'
        }
      ]
    },
    {
      label: '赞成',
      name: 'yes',
      widget: 'switch',
      activeColor: '#13ce66',
      inactiveColor: '#ff4949',
      col: 10
    },
    {
      label: '学历',
      name: 'education',
      fields: [
        {
          name: 'school',
          label: '学校',
          widget: 'input'
        },
        {
          name: 'major',
          label: '类别',
          widget: 'radio',
          options: [
            {
              value: 1,
              label: '本科'
            },
            {
              value: 2,
              label: '专科'
            }
          ]
        }
      ],
      arrayed: true
    }
  ],
  rules: {
    name: [
      {
        required: true,
        message: '请输入活动名称',
        trigger: 'blur'
      }
    ]
  },
  name: 'user',
  submit: '/api/form/add',
  row: {
    id: 1,
    name: '你好',
    dessert: 3,
    hobby: [2]
  }
};

export default [
  {
    url: '/api/form/index',
    method: 'get',
    response: () => {
      return {
        value: {
          title: '页面表单',
          view: 'Form',
          ...form
        } as View
      };
    }
  },
  {
    url: '/api/form/add',
    method: 'post',
    response: () => {
      return {
        message: '新增用户成功！'
      };
    }
  }
] as MockMethod[];
